package main

import (
	"go-config/model"
	"log"
)

const (
	defaultFilePath = "./resources/config.yaml"
)

func main() {

	filePath := defaultFilePath

	config := new(model.DemoConfig)
	config.ParseConfig(filePath)

	log.Println("Config-Map:", config.KeyMap)
	for i, b := range config.ExpectedBookings {
		log.Println("Expected:", i, b)
	}
}
