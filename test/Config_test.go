package test

import (
	"go-config/model"
	"testing"
)

func TestSimpleField(t *testing.T) {

	config := &model.DemoConfig{}
	config.ParseConfig("./test-config.yaml")

	expectedFirst := 5
	if config.Parser.LineFirstBooking != expectedFirst {
		t.Errorf("config.Parser.LineFirstBooking is %v, want: %v.", config.Parser.LineFirstBooking, expectedFirst)
	}
}

func TestArray(t *testing.T) {

	config := &model.DemoConfig{}
	config.ParseConfig("./test-config.yaml")

	expectedLenBooking := 2
	if len(config.ExpectedBookings) != expectedLenBooking {
		t.Errorf("config.ExpectedBookings is %v, want: %v.", len(config.ExpectedBookings), expectedLenBooking)
	}

	booking := config.ExpectedBookings[0]

	if booking.Limit != 200 {
		t.Errorf("First booking limit is %v, but want: %v.", booking.Limit, 200)
	}

	if booking.Name != "Kaufland" {
		t.Errorf("First booking name is %v, but want: %v.", booking.Name, "Kaufland")
	}
}

func TestMap(t *testing.T) {

	config := &model.DemoConfig{}
	config.ParseConfig("./test-config.yaml")

	expectedLenMap := 4
	if len(config.KeyMap) != expectedLenMap {
		t.Errorf("config.ExpectedBookings is %v, want: %v.", len(config.KeyMap), expectedLenMap)
	}
	v, ok := config.KeyMap["Mueller"]
	if !ok || v != "drugs" {
		t.Errorf("Expected mapping for Mueller is drugs, but got: %v, '%v'", ok, v)
	}
}
