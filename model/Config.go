package model

import (
	"log"
	"os"
	"strings"

	"gopkg.in/yaml.v3"
)

type DemoConfig struct {
	Parser struct {
		SomeString       string `yaml:"some-string"`
		LineFirstBooking int    `yaml:"line-first-booking"`
		NumbersIndex     []int  `yaml:"numbers-index"`
		NamesIndex       []int  `yaml:"names-index"`
	} `yaml:"parser"`
	KeyMap           map[string]string `yaml:"name-key-map"`
	ExpectedBookings []Booking         `yaml:"expected-bookings"`
}

type Booking struct {
	Name    string `yaml:"name"`
	Mapping string `yaml:"mapping"`
	Limit   int    `yaml:"limit"`
}

func (config *DemoConfig) ParseConfig(filePath string) {
	log.Println("Open", filePath)
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	log.Println("Parse", filePath)

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(config)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Done", filePath)
}

func (config *DemoConfig) getKey(name string) string {
	for k, v := range config.KeyMap {
		if strings.Contains(name, k) {
			return v
		}
	}
	return ""
}
